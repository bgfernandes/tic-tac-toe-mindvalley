export default class Board {
  // this creates a new board with a given size, or a new board based on an old board,
  // and a move, which is a simple object like:
  // {
  //   player: 'x',
  //   row: 1,
  //   col: 2
  // }
  constructor(boardSizeOrOldBoard, move = null) {
    let oldBoard = null;
    let boardSize = null;

    if (typeof boardSizeOrOldBoard === 'number') {
      boardSize = boardSizeOrOldBoard;
    } else if (boardSizeOrOldBoard instanceof Board) {
      oldBoard = boardSizeOrOldBoard;
      boardSize = oldBoard.map.length;

      if (move === null) {
        throw new Error('To create a new board based on an old one, a move is necessary');
      }
    } else {
      throw new Error('First parameter should be the size of a new board or a board and a move');
    }

    // sanitization
    if (boardSize < 3) {
      throw new Error('Board size should be at least 3');
    }
    if (move) {
      if ((typeof move.player !== 'string')
          || (typeof move.row !== 'number')
          || (typeof move.col !== 'number')) {
        throw new Error('A move should have a player, a row and a col');
      }

      if ((move.player !== 'x') && (move.player !== 'o')) {
        throw new Error('The only possible players are "x" and "o"');
      }

      if ((move.row < 0)
          || (move.row > boardSize - 1)
          || (move.col < 0)
          || (move.col > boardSize - 1)) {
        throw new Error('The attempted move would be outside the board');
      }
    }

    // the map is represented as an array of rows,
    // each array of rows holds positions, which hold a single character or empty string,
    // represeting which player currently holds the position, 'x' or 'o'
    this.map = [];
    for (let rowIndex = 0; rowIndex < boardSize; rowIndex += 1) {
      this.map[rowIndex] = [];
      for (let colIndex = 0; colIndex < boardSize; colIndex += 1) {
        this.map[rowIndex][colIndex] = oldBoard ? oldBoard.map[rowIndex][colIndex] : '';
      }
    }

    // if there is a move, apply it, but only if the position is unocuppied
    if (move) {
      if (this.map[move.row][move.col] !== '') {
        throw new Error('The position you are trying to move is occupied.');
      }

      this.map[move.row][move.col] = move.player;
    }
  }

  // this method tells if someone has won the game, and who
  victoriousPlayer() {
    // check all rows
    for (let rowIndex = 0; rowIndex < this.map.length; rowIndex += 1) {
      // reduces the array to an element only if it occours in every element
      const val = this.map[rowIndex].reduce((acc, next) => { return (acc === next) ? acc : ''; });
      if (val !== '') {
        return val;
      }
    }

    // check all collumns
    for (let colIndex = 0; colIndex < this.map.length; colIndex += 1) {
      // create an array with every element in the column
      const col = [];
      for (let rowIndex = 0; rowIndex < this.map.length; rowIndex += 1) {
        col.push(this.map[rowIndex][colIndex]);
      }

      // reduces the array to an element only if it occours in every element
      const val = col.reduce((acc, next) => { return (acc === next) ? acc : ''; });
      if (val !== '') {
        return val;
      }
    }

    // check crosses
    const tlBr = []; // top-left to bottom-right
    const trBl = []; // top-right to bottom-left

    for (let i = 0; i < this.map.length; i += 1) {
      tlBr.push(this.map[i][i]);
      trBl.push(this.map[i][(this.map.length - 1) - i]);
    }
    let val = tlBr.reduce((acc, next) => { return (acc === next) ? acc : ''; });
    if (val !== '') {
      return val;
    }
    val = trBl.reduce((acc, next) => { return (acc === next) ? acc : ''; });
    if (val !== '') {
      return val;
    }

    return null;
  }

  // this method lists all the possible moves for a given player
  possibleMoves(player) {
    const moves = [];

    this.map.forEach((col, rowIndex) => {
      col.forEach((pos, colIndex) => {
        if (pos === '') {
          moves.push({ player, row: rowIndex, col: colIndex });
        }
      });
    });

    return moves;
  }
}
