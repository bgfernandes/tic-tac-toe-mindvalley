import some from 'lodash/some';
import Board from './Board';

it('Should create a new board empty board of the right size when required', () => {
  const board = new Board(3);
  expect(board).toBeInstanceOf(Board);
  expect(board.map.length).toBe(3);
  board.map.forEach((row) => {
    expect(row.length).toBe(3);
    row.forEach((pos) => {
      expect(pos).toBe('');
    });
  });

  const anotherBoard = new Board(8);
  expect(anotherBoard).toBeInstanceOf(Board);
  expect(anotherBoard.map.length).toBe(8);
  anotherBoard.map.forEach((row) => {
    expect(row.length).toBe(8);
    row.forEach((pos) => {
      expect(pos).toBe('');
    });
  });
});


it('Should throw an error if the board size is under 3', () => {
  expect(() => { const b = new Board(-1); }).toThrow(); // eslint-disable-line no-unused-vars
  expect(() => { const b = new Board(0); }).toThrow(); // eslint-disable-line no-unused-vars
  expect(() => { const b = new Board(1); }).toThrow(); // eslint-disable-line no-unused-vars
  expect(() => { const b = new Board(2); }).toThrow(); // eslint-disable-line no-unused-vars
});

it('Should apply a move correctly', () => {
  let board = new Board(3);

  board = new Board(board, { player: 'x', row: 0, col: 0 });
  board.map.forEach((row, rowIndex) => {
    row.forEach((pos, colIndex) => {
      if ((rowIndex === 0) && (colIndex === 0)) {
        expect(pos).toBe('x');
      } else {
        expect(pos).toBe('');
      }
    });
  });

  board = new Board(board, { player: 'o', row: 1, col: 1 });
  board.map.forEach((row, rowIndex) => {
    row.forEach((pos, colIndex) => {
      if ((rowIndex === 0) && (colIndex === 0)) {
        expect(pos).toBe('x');
      } else if ((rowIndex === 1) && (colIndex === 1)) {
        expect(pos).toBe('o');
      } else {
        expect(pos).toBe('');
      }
    });
  });

  board = new Board(board, { player: 'x', row: 2, col: 2 });
  board.map.forEach((row, rowIndex) => {
    row.forEach((pos, colIndex) => {
      if ((rowIndex === 0) && (colIndex === 0)) {
        expect(pos).toBe('x');
      } else if ((rowIndex === 1) && (colIndex === 1)) {
        expect(pos).toBe('o');
      } else if ((rowIndex === 2) && (colIndex === 2)) {
        expect(pos).toBe('x');
      } else {
        expect(pos).toBe('');
      }
    });
  });
});

it('Should reject an incorrect move', () => {
  let board = new Board(3);

  expect(() => {
    board = new Board(board, { player: '?', row: 0, col: 0 });
  }).toThrow();

  board = new Board(3);
  expect(() => {
    board = new Board(board, { player: 'x', row: -1, col: 0 });
  }).toThrow();

  board = new Board(3);
  expect(() => {
    board = new Board(board, { player: 'x', row: 1, col: -1 });
  }).toThrow();

  board = new Board(3);
  expect(() => {
    board = new Board(board, { player: 'x', row: 3, col: 1 });
  }).toThrow();

  board = new Board(3);
  expect(() => {
    board = new Board(board, { player: 'x', row: 1, col: 3 });
  }).toThrow();

  board = new Board(3);
  board = new Board(board, { player: 'x', row: 1, col: 1 });
  expect(() => {
    board = new Board(board, { player: 'o', row: 1, col: 1 });
  }).toThrow();
});

it('Should tell when a player has won or not', () => {
  let board = new Board(3);

  expect(board.victoriousPlayer()).toBe(null);

  board = new Board(board, { player: 'x', row: 0, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);
  board = new Board(board, { player: 'x', row: 1, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);

  board = new Board(board, { player: 'x', row: 2, col: 0 });
  expect(board.victoriousPlayer()).toBe('x');

  board = new Board(3);
  board = new Board(board, { player: 'o', row: 0, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);
  board = new Board(board, { player: 'o', row: 0, col: 1 });
  expect(board.victoriousPlayer()).toBe(null);

  board = new Board(board, { player: 'o', row: 0, col: 2 });
  expect(board.victoriousPlayer()).toBe('o');

  board = new Board(3);
  board = new Board(board, { player: 'o', row: 0, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);
  board = new Board(board, { player: 'o', row: 1, col: 1 });
  expect(board.victoriousPlayer()).toBe(null);

  board = new Board(board, { player: 'o', row: 2, col: 2 });
  expect(board.victoriousPlayer()).toBe('o');

  board = new Board(3);
  board = new Board(board, { player: 'x', row: 0, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);
  board = new Board(board, { player: 'o', row: 1, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);
  board = new Board(board, { player: 'x', row: 2, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);

  board = new Board(3);
  board = new Board(board, { player: 'o', row: 0, col: 0 });
  expect(board.victoriousPlayer()).toBe(null);
  board = new Board(board, { player: 'x', row: 1, col: 1 });
  expect(board.victoriousPlayer()).toBe(null);
  board = new Board(board, { player: 'o', row: 2, col: 2 });
  expect(board.victoriousPlayer()).toBe(null);

  board = new Board(3);
  board = new Board(board, { player: 'x', row: 0, col: 2 });
  board = new Board(board, { player: 'x', row: 1, col: 1 });
  board = new Board(board, { player: 'x', row: 2, col: 0 });
  expect(board.victoriousPlayer()).toBe('x');
});

it('Should list all possible moves available for a given player', () => {
  let board = new Board(3);
  let possibleMoves = board.possibleMoves('o');
  expect(possibleMoves.length).toBe(9);
  expect(some(possibleMoves, { player: 'o', row: 0, col: 0 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 0, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 0, col: 2 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 1, col: 0 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 1, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 1, col: 2 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 2, col: 0 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 2, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 2, col: 2 })).toBe(true);

  board = new Board(board, { player: 'o', row: 0, col: 0 });
  possibleMoves = board.possibleMoves('x');
  expect(possibleMoves.length).toBe(8);
  expect(some(possibleMoves, { player: 'x', row: 0, col: 0 })).toBe(false);
  expect(some(possibleMoves, { player: 'x', row: 0, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'x', row: 0, col: 2 })).toBe(true);
  expect(some(possibleMoves, { player: 'x', row: 1, col: 0 })).toBe(true);
  expect(some(possibleMoves, { player: 'x', row: 1, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'x', row: 1, col: 2 })).toBe(true);
  expect(some(possibleMoves, { player: 'x', row: 2, col: 0 })).toBe(true);
  expect(some(possibleMoves, { player: 'x', row: 2, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'x', row: 2, col: 2 })).toBe(true);

  board = new Board(board, { player: 'x', row: 1, col: 0 });
  possibleMoves = board.possibleMoves('o');
  expect(possibleMoves.length).toBe(7);
  expect(some(possibleMoves, { player: 'o', row: 0, col: 0 })).toBe(false);
  expect(some(possibleMoves, { player: 'o', row: 0, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 0, col: 2 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 1, col: 0 })).toBe(false);
  expect(some(possibleMoves, { player: 'o', row: 1, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 1, col: 2 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 2, col: 0 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 2, col: 1 })).toBe(true);
  expect(some(possibleMoves, { player: 'o', row: 2, col: 2 })).toBe(true);
});
