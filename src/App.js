import React, { Component } from 'react';
import styled from 'styled-components';

import Board from './models/Board';
import OptionsComponent from './components/OptionsComponent';
import BoardComponent from './components/BoardComponent';
import minMax, { getCounter, resetCounter } from './MinMax';

const Container = styled.div``;
const Header = styled.div``;
const Paragraph = styled.p``;

class App extends Component {
  constructor() {
    super();

    // this is the main state of the application
    // I decided not to use redux since this is a very simple application
    this.state = {
      aiIsThinking: false,
      board: null,
      player: null,
      message: null,
      minMaxDepth: null,
    };

    this.startNewGame = this.startNewGame.bind(this);
    this.onBoardClick = this.onBoardClick.bind(this);
  }

  startNewGame(boardSize, player, minMaxDepth) {
    const board = new Board(Number(boardSize));
    this.setState({
      board,
      player,
      minMaxDepth: Number(minMaxDepth),
    });

    // if the player is 'o', the AI moves first
    if (player === 'o') {
      // heuristic: when AI is first, always play on top left corner:
      const newBoard = new Board(board, { player: 'x', row: 0, col: 0 });
      this.setState({ board: newBoard });
    }
  }

  friendlyStatus() {
    const { board, player, aiIsThinking } = this.state;

    if (board === null) {
      return 'Waiting to begin.';
    }

    if (aiIsThinking === true) {
      return "I'm thinking!";
    }

    const victoriousPlayer = board.victoriousPlayer();
    if (victoriousPlayer === player) {
      return 'The player has won!'; // this shouldn't happen :D
    } else if (victoriousPlayer) {
      return 'The AI has won!';
    } else if (board.possibleMoves(player).length === 0) {
      return 'The game is a draw.';
    }

    return 'Waiting for your move';
  }

  onBoardClick(row, col) {
    const { player, board, aiIsThinking, minMaxDepth } = this.state;

    // don't let the player do anything if the AI is still thinking
    // or if the game is already over
    if (aiIsThinking || board.victoriousPlayer()) {
      return;
    }

    // check if the match is already drawed
    if (board.possibleMoves(player).length === 0) {
      return;
    }

    // applies the player move to create a new board
    let newBoard;
    try {
      newBoard = new Board(board, { player, row, col });
    } catch (e) {
      this.setState({
        message: e.message,
      });
      return;
    }

    this.setState({
      board: newBoard,
      message: null,
    });

    // checks if the player has won or if it's a draw
    if (newBoard.victoriousPlayer()) {
      return;
    } else if (newBoard.possibleMoves().length === 0) {
      return;
    }

    this.executeAiPlayerMove(newBoard, player, minMaxDepth);
  }

  async executeAiPlayerMove(board, player, minMaxDepth) {
    this.setState({
      aiIsThinking: true,
    });

    // the AI will explore the tree of
    // possible moves and chose a move where it can win
    const aiPlayer = player === 'x' ? 'o' : 'x';
    const possibleAiMoves = board.possibleMoves(aiPlayer);
    const movesWithScore = [];

    // reset the counter of how many times minMax function is called
    resetCounter();

    // match each move with a score
    await Promise.all(possibleAiMoves.map(async (move) => {
      const score = await minMax({
        board,
        move,
        aiPlayer,
        depth: minMaxDepth });

      movesWithScore.push({ move, score });
    }));

    // finds the move with biggest score
    const bestMoveWithScore = movesWithScore.reduce((best, current) => {
      if (current.score > best.score) {
        return current;
      }
      return best;
    });

    // executes the move
    this.setState({
      board: new Board(board, bestMoveWithScore.move),
      aiIsThinking: false,
    });
  }

  render() {
    return (
      <Container>

        <Header>
          <Paragraph>
            This application applies the <a href="https://en.wikipedia.org/wiki/Minimax">MiniMax</a> algorithm to a tic-tac-toe game.
          </Paragraph>
          <Paragraph>
            Note that with bigger game boards, the algorithm takes exponentially more time to
            decide on a move, since it considers every board arrangement possible in the game.
          </Paragraph>
        </Header>

        <OptionsComponent startNewGame={this.startNewGame} />

        <Paragraph>
          Game Status: {this.friendlyStatus()}
        </Paragraph>

        <Paragraph>
          The last AI move took {getCounter()} call(s) to the MinMax function.
        </Paragraph>

        {this.state.message ? <Paragraph>{this.state.message}</Paragraph> : null}

        <BoardComponent board={this.state.board} onClick={this.onBoardClick} />

      </Container>
    );
  }
}

export default App;
