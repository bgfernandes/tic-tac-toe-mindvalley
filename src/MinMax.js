// this is an adaptation of the MiniMax algorithm: https://en.wikipedia.org/wiki/Minimax
// this is the point where I regret a bit about choosing javascript for this challenge
// since it's not multithreaded, I'll add a shameful workaround to give a semblance
// of responsivity to the UI

import Board from './models/Board';

// awaits for a single millisecond, enough so that the main thread can answer to some of the UI
function shamefulWorkaround() {
  return new Promise((resolve) => {
    setTimeout(resolve, 0);
  });
}

let counter = 0;

export function getCounter() {
  return counter;
}

export function resetCounter() {
  counter = 0;
}

export default async function minMax({ board, move, aiPlayer, depth }) {
  await shamefulWorkaround();

  counter += 1;

  if (depth === 0) {
    return 0;
  }

  const newBoard = new Board(board, move);

  const victoriousPlayer = newBoard.victoriousPlayer();
  if ((victoriousPlayer === aiPlayer)) {
    return 1; // the ai has won
  } else if (victoriousPlayer !== null) {
    return -1; // the human player has won
  }

  const nextPlayer = move.player === 'x' ? 'o' : 'x';

  const nextMoves = newBoard.possibleMoves(nextPlayer);
  if (nextMoves.length === 0) {
    return 0; // it's a draw
  }

  const moveEvaluations = [];

  for (let i = 0; i < nextMoves.length; i += 1) {
    // I conciently want to do this sequencially to search the node tree by depth
    // eslint-disable-next-line no-await-in-loop
    const score = await minMax({
      board: newBoard,
      move: nextMoves[i],
      aiPlayer,
      depth: depth - 1 });

    if ((nextPlayer !== aiPlayer) && (score === -1)) {
      // if I'm minimizing and I find a -1, stop here and don't look down the tree any further
      return -1;
    } else if ((nextPlayer === aiPlayer) && (score === 1)) {
      // if I'm maximizing and I find a 1, stop here and don't look down the tree any further
      return 1;
    }

    moveEvaluations.push(score);
  }

  // if the next player is not the ai, he wants the worst score
  if (nextPlayer !== aiPlayer) {
    return Math.min(...moveEvaluations);
  } // if next player is the ai, he wants the best one
  return Math.max(...moveEvaluations);
}


