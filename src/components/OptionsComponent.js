import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.div``;
const Paragraph = styled.p``;
const BoardSizeInput = styled.input`
  width: 40px;
`;
const StartNewGameButton = styled.button``;
const PlayerSelect = styled.select``;
const DepthInput = styled.input`
  width: 40px;
`;

class OptionsComponent extends Component {
  constructor() {
    super();

    this.state = {
      boardSize: 3,
      player: 'x',
      minMaxDepth: 7,
    };

    this.onChanngeBoardSize = this.onChanngeBoardSize.bind(this);
    this.onChangePlayer = this.onChangePlayer.bind(this);
    this.onChangeDepth = this.onChangeDepth.bind(this);
  }

  onChanngeBoardSize(e) {
    this.setState({
      boardSize: e.target.value,
    });
  }

  onChangePlayer(e) {
    this.setState({
      player: e.target.value,
    });
  }

  onChangeDepth(e) {
    this.setState({
      minMaxDepth: e.target.value,
    });
  }

  render() {
    return (
      <Container>
        <Paragraph>
          Start a new game: <br/>

          Board Size:
          <BoardSizeInput
            type='number'
            value={this.state.boardSize}
            onChange={this.onChanngeBoardSize} /><br/>

          Select a Player:
          <PlayerSelect value={this.state.player} onChange={this.onChangePlayer}>
            <option value='x'> x </option>
            <option value='o'> o </option>
          </PlayerSelect><br/>

          MinMax max depth:
          <DepthInput
            type='number'
            value={this.state.minMaxDepth}
            onChange={this.onChangeDepth} /><br/>

          <StartNewGameButton
            onClick={() =>
              this.props.startNewGame(
                this.state.boardSize,
                this.state.player,
                this.state.minMaxDepth) }>
            Start a New Game
          </StartNewGameButton>
        </Paragraph>

      </Container>
    );
  }
}

OptionsComponent.propTypes = {
  startNewGame: PropTypes.func.isRequired,
};

export default OptionsComponent;
