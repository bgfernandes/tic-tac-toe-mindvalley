import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Box = styled.div`
  display: inline-block;
  border-style: solid;
  border-width: 1px;
`;
const TouchableArea = styled.div`
  display: inline-block;
  cursor: pointer;
  margin: 10px;
  padding: 10px 10px;
  font-family: "Lucida Console", Monaco, monospace;
`;

function PositionComponent({ content, row, col, onClick }) {
  return (
    <Box>
      <TouchableArea onClick={() => onClick(row, col)}>
        {content === '' ? '\u00A0' : content }
      </TouchableArea>
    </Box>
  );
}

PositionComponent.propTypes = {
  content: PropTypes.string.isRequired,
  row: PropTypes.number.isRequired,
  col: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default PositionComponent;
