import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Board from './../models/Board';
import PositionComponent from './PositionComponent';

const Paragraph = styled.p``;
const Box = styled.div`
  border-style: solid;
  border-width: 1px;
  display: inline-block;
`;
const Line = styled.div``;

function BoardComponent({ board, onClick }) {
  if (!board) {
    return (
      <Paragraph>
        No board to display.
      </Paragraph>
    );
  }

  // this is odd, calling the map function on the map property of board
  const lines = board.map.map((row, rowIndex) => {
    // for each position in the line, create a PositionComponent
    const linePositions = row.map((pos, colIndex) => {
      return <PositionComponent
        key={`${rowIndex},${colIndex}`}
        content={pos}
        row={rowIndex}
        col={colIndex}
        onClick={onClick}
        />;
    });

    return (
    <Line key={rowIndex}>
      {linePositions}
    </Line>
    );
  });

  return (
    <Box>
      {lines}
    </Box>
  );
}

BoardComponent.propTypes = {
  board: PropTypes.instanceOf(Board),
  onClick: PropTypes.func.isRequired,
};

export default BoardComponent;
