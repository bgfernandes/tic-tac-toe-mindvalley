# tic-tac-toe-mindvalley

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Run `npm install` to install depedencies.

Run `npm start` to run on dev mode.

Run `npm run build` to generate a production distribution.

Run `npm test` to run the test cases.
